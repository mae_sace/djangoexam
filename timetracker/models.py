from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from django.db.models import Sum, signals
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.core.validators import RegexValidator

import datetime
from django.utils import timezone

# model for all Projects
@python_2_unicode_compatible # only if you need to support Python 2
class Project(models.Model):
    project_name = models.CharField(max_length=200)

    def get_total_hours(self):
        return self.timeentry_set.all().aggregate(Sum('time_entry_duration'))['time_entry_duration__sum']

    get_total_hours.short_description = 'Total hours'

    def __str__(self):
        return self.project_name

def seconds_to_hours(td):
    if td is None:
        return 0
    days, seconds = td.days, td.seconds
    return days * 24 + seconds // 3600

def str_to_date(str):
    return datetime.datetime.strptime(str, "%Y-%m-%d").date()

@python_2_unicode_compatible # only if you need to support Python 2
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def total_hours_for_day(self, date):
        sum =  self.timeentry_set.all().filter(time_entry_date=date).aggregate(Sum('time_entry_duration'))['time_entry_duration__sum']
        return "{} hrs".format(seconds_to_hours(sum))

    def total_hours_for_week(self, date):
        start = date - datetime.timedelta(days=date.weekday())
        end = start + datetime.timedelta(days=6)
        sum =  self.timeentry_set.all().filter(time_entry_date__gte=start,time_entry_date__lte=end).aggregate(Sum('time_entry_duration'))['time_entry_duration__sum']
        return "{} hrs".format(seconds_to_hours(sum))

    def total_hours_for_month(self, date):
        sum =  self.timeentry_set.all().filter(time_entry_date__month=date.month,time_entry_date__year=date.year).aggregate(Sum('time_entry_duration'))['time_entry_duration__sum']
        return "{} hrs".format(seconds_to_hours(sum))

    def total_hours(self,date):
        day = self.total_hours_for_day(date)
        week = self.total_hours_for_week(date)
        month = self.total_hours_for_month(date)
        return {'day':day, 'week':week, 'month':month}

    def __str__(self):
        return self.user.username

# create profile when user is created
@receiver(signals.post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

# create profile when user is created
@receiver(signals.post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


# model for all Time Entries
@python_2_unicode_compatible # only if you need to support Python 2
class TimeEntry(models.Model):
    time_entry_date = models.DateField(verbose_name="Date")
    time_entry_duration = models.DurationField(help_text="HH:MM:SS",verbose_name="Duration")
    remarks = models.CharField(max_length=200,default=None, blank=True, null=True)
    project = models.ForeignKey(Project,on_delete=models.SET_NULL,null=True,blank=True)
    profile = models.ForeignKey(Profile,on_delete=models.CASCADE)
    created_date = models.DateTimeField(default=timezone.now())

    def is_late_log(self):
        now = datetime.date.today()
        return self.time_entry_date < now

    def __str__(self):
        if self.remarks:
            return self.remarks
        else:
            return self.profile.user.username + "'s entry log"