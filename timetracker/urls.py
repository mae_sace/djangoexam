from django.conf.urls import url, include
from . import views

from django.contrib.auth.views import LogoutView, LoginView

app_name = "timetracker"

# ajax urls
ajax_patterns = [
    url(r'^validate_duration/$',views.validate_duration, name="validate_duration"),
    url(r'^(?P<pk>[0-9]+)/',include([
        url(r'^update/$',views.update_time_entry, name="update"),
        url(r'^delete/$',views.DeleteTimeEntry.as_view(),name="delete"),
    ]))
]

# entry log related urls
log_patterns = [
    url(r'^$',views.TimeEntryDetail.as_view(),name="detail"),
    url(r'^update/$',views.UpdateTimeEntry.as_view(),name="update"),
]

# base urls
urlpatterns = [
    url(r'^$',views.CreateTimeEntry.as_view(), name="user-log"),
    url(r'^(?P<pk>[0-9]+)/', include(log_patterns, namespace="log")),
    url(r'^ajax/', include(ajax_patterns, namespace="ajax")),
]