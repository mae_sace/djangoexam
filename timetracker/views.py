from django.shortcuts import render,get_object_or_404,reverse
from django.http import HttpResponse,HttpResponseRedirect,JsonResponse
from django.views import generic
from django.urls import reverse_lazy
from django.db import transaction

from .models import Project,TimeEntry,Profile
from .forms import TimeEntryForm, TimeEntryFormSet
from django.contrib.auth.forms import UserCreationForm

from braces.views import JSONResponseMixin, AjaxResponseMixin

from django.contrib.auth.mixins import LoginRequiredMixin

from datetime import datetime, timedelta, time

from django.utils.dateparse import parse_duration,parse_date
from django.utils.encoding import force_text

class TimeEntryDetail(LoginRequiredMixin,generic.DetailView):
    model = TimeEntry
    context_object_name = 'timeentry'

class CreateTimeEntry(LoginRequiredMixin,generic.CreateView):
    model = TimeEntry
    form_class = TimeEntryForm
    success_url = reverse_lazy('timetracker:user-log')

    def form_valid(self, form):
        """
        Include profile id in form data
        """
        userid = self.request.user.id
        profile = Profile.objects.get(user__id=userid)
        form.instance.profile = profile
        return super(CreateTimeEntry, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        userid = self.request.user.id
        context['user_log'] = TimeEntry.objects.filter(profile__id=userid).order_by('time_entry_date')
        if 'filter_date' in self.request.GET and self.request.GET['filter_date'] != '':
            context['filtered'] = True
            filterdate = parse_date(self.request.GET['filter_date'])
            profile = Profile.objects.get(user_id=userid)
            context['total_hours'] = profile.total_hours(date=filterdate)
            context['user_log'] = context['user_log'].filter(created_date__year=filterdate.year,created_date__month=filterdate.month,created_date__day=filterdate.day).order_by('time_entry_date')
        else:
            context['filtered'] = False
        return context

class UpdateTimeEntry(LoginRequiredMixin,AjaxResponseMixin,JSONResponseMixin,generic.UpdateView):
    model = TimeEntry
    form_class = TimeEntryForm
    template_name = "timetracker/edit_entry_log.html"
    success_url = reverse_lazy('timetracker:user-log')

class DeleteTimeEntry(LoginRequiredMixin,JSONResponseMixin, AjaxResponseMixin,generic.DeleteView):
    model = TimeEntry
    success_url = reverse_lazy('timetracker:user-log')
    
    def delete_ajax(self, request, *args, **kwargs):
        json_dict = {}
        return self.render_json_response(json_dict)

def validate_duration(request):
    if not request.POST:
        return JsonResponse({})
    duration = request.POST.get('time_entry_duration', None)
    data = {
        'is_valid' : isinstance(parse_duration(duration), timedelta)
    }
    return JsonResponse(data)

def update_time_entry(request,pk):
    if not request.POST:
        return JsonResponse({})
    timeentry = get_object_or_404(TimeEntry,pk=pk)
    name = request.POST.get('name',None)
    value = request.POST.get(name,None)
    val1 = None
    total_hours = None
    if name == "time_entry_date":
        timeentry.time_entry_date = datetime.strptime(value, "%Y-%m-%d").date()
        val1 = value
        timeentry.save()
        profile = timeentry.profile
        total_hours = profile.total_hours(date=timeentry.time_entry_date)
    elif name == 'time_entry_duration':
        timeentry.time_entry_duration = parse_duration(value)
        val1 = timeentry.time_entry_duration
    elif name == 'remarks':
        timeentry.remarks = value
        val1 = value
    elif name == 'project':
        if value:
            project = Project.objects.get(pk=value)
            val1 = project.project_name
        else:
            project = None
            val1 = 'None'
        timeentry.project = project
    timeentry.save()
    data = {
        'name' : name,
        'value' : val1,
        'total_hours' : total_hours
    }
    return JsonResponse(data)

class SignUp(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'

def logout_view(request):
    return render(request,'registration/logout.html')