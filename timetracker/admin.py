from django.contrib import admin

from .models import Project, TimeEntry, Profile

class ProjectAdmin(admin.ModelAdmin):
    list_display = ('project_name', 'get_total_hours')

admin.site.register(Project,ProjectAdmin)
admin.site.register(TimeEntry)