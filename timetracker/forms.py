from django import forms

from .models import TimeEntry, Project, Profile

class TimeEntryForm(forms.ModelForm):
    
    class Meta:
        model = TimeEntry
        fields = ('time_entry_date','time_entry_duration','remarks','project')
        widgets = {
            'time_entry_date' : forms.TextInput(attrs={'class':'datepicker','placeholder':'Date'}),
            'time_entry_duration' : forms.TextInput(attrs={'class':'duration_field','placeholder':'HH:MM:SS'}),
            'remarks' : forms.TextInput(attrs={'placeholder':'Remarks'}),
            'project' : forms.Select(attrs={'placeholder':'Project'}),
        }

TimeEntryFormSet = forms.inlineformset_factory(Profile, TimeEntry, form=TimeEntryForm, extra=1)