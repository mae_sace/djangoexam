from django import template

register = template.Library()

# Based from
# https://stackoverflow.com/questions/33105457/display-and-format-django-durationfield-in-template#answer-33106384
@register.filter(name='format_duration_to_hours')
def format_duration_to_hours(td):
    days, seconds = td.days, td.seconds
    hours = days * 24 + seconds // 3600
    minutes = ((seconds % 3600) // 60) // 60

    return '{:03.02f} hours {:03.02f} minutes'.format(hours,minutes)

@register.filter(name='replace_string')
def replace_string(str,find_str):
    """
    Place a string placeholder in specified string
    """
    return str.replace(find_str,"%s")

@register.filter
def format_string(str, str2):
    """
    Replace string placeholder with specified string
    """
    if "%s" in str:
        return str.replace("%s",str2)
    else:
        return str